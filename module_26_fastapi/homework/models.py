from database import Base
from sqlalchemy import Column, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship


class Recipe(Base):
    __tablename__ = "Recipe"

    name = Column(String, primary_key=True, index=True)
    views_amt = Column(Integer, index=True, nullable=False, default=0)
    cooking_time = Column(Integer, index=True, nullable=False)

    ingredients = relationship(
        "RecipeInfo", back_populates="recipe", cascade="all, delete-orphan"
    )


class RecipeInfo(Base):
    __tablename__ = "RecipeInfo"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(ForeignKey("Recipe.name"), nullable=False)
    ingredients = Column(Text, index=True, nullable=False)
    info = Column(Text, index=True, nullable=False)

    recipe = relationship("Recipe", back_populates="ingredients")

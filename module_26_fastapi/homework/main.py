from typing import List

import models
import schemas
from database import engine, session
from fastapi import FastAPI, Path
from sqlalchemy import desc, update
from sqlalchemy.future import select

app = FastAPI()


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()


@app.get("/recipes", response_model=List[schemas.Recipe])
async def recipes() -> List[models.Recipe]:
    await session.execute(
        update(models.Recipe).values(views_amt=models.Recipe.views_amt + 1)
    )
    res = await session.execute(
        select(models.Recipe).order_by(
            desc(models.Recipe.views_amt), desc(models.Recipe.cooking_time)
        )
    )
    return res.scalars().all()


@app.get("/recipes/{name}/ingredients", response_model=List[str])
async def ingredients(name: str = Path(..., title="Name of the recipe")) -> list[str]:
    res = await session.execute(
        select(models.RecipeInfo).where(models.RecipeInfo.name == name)
    )
    (recipe_info,) = res.fetchone()
    return str(recipe_info.ingredients).split(", ")


@app.get("/recipes/info", response_model=List[schemas.RecipeInfo])
async def info() -> list[schemas.RecipeInfo]:
    res = await session.execute(select(models.RecipeInfo))

    return res.scalars().all()

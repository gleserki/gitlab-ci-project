import pytest


@pytest.mark.parametrize(
    "route", ["/recipes", "/recipes/eggs/ingredients", "/recipes/info"]
)
def test_routes_status(client, route):
    rv = client.get(route)
    assert rv.status_code == 200

from typing import List

from pydantic import BaseModel, validator


class Recipe(BaseModel):
    name: str
    views_amt: int
    cooking_time: int

    class Config:
        orm_mode = True


class RecipeInfo(BaseModel):
    name: str
    ingredients: List[str]
    info: str

    @validator("ingredients", pre=True)
    def ingredients_to_list(cls, value):
        return value.split(", ")

    class Config:
        orm_mode = True
